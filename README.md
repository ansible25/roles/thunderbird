Thunderbird
=========

Role that installs Thunderbird email client

Requirements
------------

None

Role Variables
--------------

None

Dependencies
------------

None

Example Playbook
----------------

Installing Thunderbird on targeted machine:

    - hosts: servers
      roles:
         - thunderbird

License
-------

[MIT](LICENSE)

Author Information
------------------

This role was created by [bradthebuilder](https://bradthebuilder.me)
